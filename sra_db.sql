-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 08, 2016 at 04:59 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sra_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_property`
--

CREATE TABLE IF NOT EXISTS `tb_property` (
  `place_id` int(11) NOT NULL AUTO_INCREMENT,
  `apt_num` varchar(20) DEFAULT NULL,
  `street_num` varchar(50) NOT NULL,
  `street_route` varchar(200) NOT NULL,
  `suburb` varchar(100) NOT NULL,
  `aus_state` varchar(50) DEFAULT NULL,
  `zip_code` int(20) NOT NULL,
  `country_aus` varchar(30) NOT NULL,
  `id` int(11) DEFAULT NULL,
  `lat` float(15,11) DEFAULT NULL,
  `lng` float(15,11) DEFAULT NULL,
  PRIMARY KEY (`place_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `tb_property`
--

INSERT INTO `tb_property` (`place_id`, `apt_num`, `street_num`, `street_route`, `suburb`, `aus_state`, `zip_code`, `country_aus`, `id`, `lat`, `lng`) VALUES
(30, '5', '5', 'Tasman Street', 'Preston', 'VIC', 3072, 'Australia', 38, NULL, NULL),
(31, '', '81', 'Chapel Street', 'Windsor', 'VIC', 3181, 'Australia', 38, NULL, NULL),
(32, '', '12', 'Elizabeth Street', 'Melbourne', 'VIC', 3000, 'Australia', 38, NULL, NULL),
(33, '', '108', 'Valley Drive', 'Wallan', 'VIC', 3756, 'Australia', 38, NULL, NULL),
(34, '', '78', 'Chapel Street', 'Windsor', 'VIC', 3181, 'Australia', 38, NULL, NULL),
(35, NULL, '5', 'Oakwood Road', 'Albanvale', 'VIC', 3021, 'Australia', 38, NULL, NULL),
(36, NULL, '55', 'Oakwood Road', 'Albanvale', 'VIC', 3021, 'Australia', 38, NULL, NULL),
(37, NULL, '55', 'Oakwood Road', 'Albanvale', 'VIC', 3021, 'Australia', 38, NULL, NULL),
(38, NULL, '55', 'Oakwood Road', 'Albanvale', 'VIC', 3021, 'Australia', 38, NULL, NULL),
(39, '', '584', 'Lonsdale Street', 'Melbourne', 'VIC', 3000, 'Australia', 38, NULL, NULL),
(40, NULL, '78', 'Rennie Street', 'Coburg', 'VIC', 3058, 'Australia', 38, NULL, NULL),
(41, NULL, '564', 'Malvern Road', 'Prahran', 'VIC', 3181, 'Australia', 38, NULL, NULL),
(42, '', '55', 'Dundas Place', 'Albert Park', 'VIC', 3206, 'Australia', 38, NULL, NULL),
(43, '', '104', 'Perth Avenue', 'Albion', 'VIC', 3020, 'Australia', 38, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `join_date` datetime NOT NULL,
  `psalt` text NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `r_e_name` varchar(150) DEFAULT NULL,
  `prop_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `fullname`, `email`, `join_date`, `psalt`, `user_type`, `r_e_name`, `prop_count`) VALUES
(38, 'GaganjotK', '6870cf4c68b590be6eb02bfd61f48529ca883bb07ca3b40908157682213f6f4e', 'Gaganjot Kaur', 'gaganjot@yahoo.com', '2016-02-10 18:18:21', '5b5c5424c0fa0c0', 'Tenant', NULL, 14);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_property`
--
ALTER TABLE `tb_property`
  ADD CONSTRAINT `tb_property_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
