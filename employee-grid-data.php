<?php
/* Database connection start */
require ("common.php");
/* Database connection end */


// storing  request (ie, get/post) global array to a variable  
$requestData= $_REQUEST;

$userID= $_SESSION['user']['id'];


$columns = array( 
// datatable column index  => database column name
	0 =>'apt_num', 
	1 => 'street_num',
	2=> 'street_route',
	3=>'suburb',
	4=>'aus_state',
	5=>'zip_code'
);

// getting total number records without any search
$sql = "SELECT  apt_num, street_num, street_route,suburb,aus_state,zip_code FROM tb_property WHERE id=$userID";

$query = $db->prepare($sql);
$query->execute();
$totalData = $query->rowCount();
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.


$sql = "SELECT apt_num, street_num, street_route,suburb,aus_state,zip_code  ";
$sql.=" FROM tb_property WHERE 1=1 && id=$userID";
if( !empty($requestData['search']['value']) ) {   // if there is a search parameter, $requestData['search']['value'] contains search parameter
	$sql.=" AND ( apt_num LIKE '".$requestData['search']['value']."%' ";    
	$sql.=" OR street_num LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR street_route LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR suburb LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR aus_state LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR zip_code LIKE '".$requestData['search']['value']."%' )";
}
$query=$db->prepare($sql);
$query->execute();
$totalFiltered = $query->rowCount(); // when there is a search parameter then we have to modify total number filtered rows as per search result. 
$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."  LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
/* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc  */	
$query=$db->prepare($sql);
$query->execute();
$data = array();
while( $row=$query->fetch() ) {  // preparing an array
	$nestedData=array(); 

	$nestedData[] = $row["apt_num"];
	$nestedData[] = $row["street_num"];
	$nestedData[] = $row["street_route"];
	$nestedData[] = $row["suburb"];
	$nestedData[] = $row["aus_state"];
	$nestedData[] = $row["zip_code"];
	$data[] = $nestedData;
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);

echo json_encode($json_data);  // send data as json format

?>
