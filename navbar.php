	<!-- NAVBAR
================================================== -->
  
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">Sustainable Renting</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#rent_green">Rent Green</a></li>
                <li>
                	<?php if(isset($_SESSION['user'])) : ?>	
                  <a href="add_property.php">Add Green Property </a>
                  	<?php else : ?>
                  		<?php endif; ?>
                </li>
                <li><a href="#contact">Contact</a></li>
                <!--<li><?php if(isset($_SESSION['logged_in'])) : ?>
	<?php $user = unserialize($_SESSION['user']); ?>
	Hello, <?php echo $user->username; ?>.  
	<a href="logout.php">Logout</a> | <a href="account_settings.php">Change Email</a>
<?php else : ?>
	<a href="login.php">Log In</a> </li>
	<li><a href="sign_up.php">Sign Up</a></li>
<?php endif; ?>-->
              </ul>
              <ul class="nav pull-right navbar-nav">          
          <li class="dropdown">
          	<?php if(isset($_SESSION['user'])) : ?>	
	<!--G'Day, <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?>-->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> <i class="glyphicon glyphicon-chevron-down"></i></a>
            <ul class="dropdown-menu">
            	<li><a href="user_profile.php">Profile</a></li>
            	<li><a href="changePassword.php">Change Password</a></li>
            	<li><a href="#">Added Properties</a></li>
            	<li><a href="logout.php"> Logout</a></li>
            <?php else : ?>
            	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i> <i class="glyphicon glyphicon-chevron-down"></i></a>
            <ul class="dropdown-menu">
            <li>	<a href="register.php"> Sign Up</a></li>
              <li><a href="login.php"> Login</a></li>
            
              <?php endif; ?>
             </ul>
          </li>  
        </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
    
    
    
