<?php
//if we got something through $_POST
if (isset($_POST['search'])) {
    // here you would normally include some database connection
    require("common.php");
    
    $searchword = htmlentities($_POST['search']);
	
	$comma = ",";
	
	$findcomma = strpos($searchword, $comma);
	
	if($findcomma!==FALSE){
		// split the phrase by any number of commas or space characters,
		// which include " ", \r, \t, \n and \f
		// For splitting the String into an array		
		$keywords = preg_split("/[,]+/", $searchword);
		
		// For suburb
		$keywords[0];
			
		
		// build your search query to the database
    	$sql = "SELECT * FROM tb_property WHERE suburb LIKE '%{$keywords[0]}%' LIMIT 10";
    	// get results
    	$row = $db->prepare($sql);
		$row->execute();
		$rows= $row->fetchAll();
    	if(count($rows)) {
        $end_result = '';
        foreach($rows as $r) {
        	if(empty($r['apt_num'])){
        		$result         = $r['street_num'].','.$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $keywords[0] . '</span>';    
            	$end_result     .= '<li>' . str_ireplace($keywords[0], $bold, $result) . '</li>';
        	}else{
        		$result         = $r['apt_num'].'/'.$r['street_num'].','.
        		$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $keywords[0] . '</span>';    
            	$end_result     .= '<li>' . str_ireplace($keywords[0], $bold, $result) . '</li>';	
        	}                       
        }
        echo $end_result;
    	}else {	
        echo '<li>No results found</li>';
    	}
			
	}elseif(is_numeric($searchword)){
		// build your search query to the database
    	$sql = "SELECT * FROM tb_property WHERE zip_code LIKE '%{$searchword}%' LIMIT 10";
    	// get results
    	$row = $db->prepare($sql);
		$row->execute();
		$rows= $row->fetchAll();
    	if(count($rows)) {
        $end_result = '';
        foreach($rows as $r) {
            if(empty($r['apt_num'])){
        		$result         = $r['street_num'].','.$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $searchword. '</span>';    
            	$end_result     .= '<li>' . str_ireplace($searchword, $bold, $result) . '</li>';
        	}else{
        		$result         = $r['apt_num'].'/'.$r['street_num'].','.
        		$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $searchword . '</span>';    
            	$end_result     .= '<li>' . str_ireplace($searchword, $bold, $result) . '</li>';	
        	}             
        }
        echo $end_result;
    	}else {	
        echo '<li>No results found</li>';
    	}
		
	}else{		
		// build your search query to the database
    	$sql = "SELECT * FROM tb_property WHERE suburb LIKE '%{$searchword}%' LIMIT 10";
    	// get results
    	$row = $db->prepare($sql);
		$row->execute();
		$rows= $row->fetchAll();
    	if(count($rows)) {
        $end_result = '';
        foreach($rows as $r) {
            if(empty($r['apt_num'])){
        		$result         = $r['street_num'].','.$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $searchword . '</span>';    
            	$end_result     .= '<li>' . str_ireplace($searchword, $bold, $result) . '</li>';
        	}else{
        		$result         = $r['apt_num'].'/'.$r['street_num'].','.
        		$r['suburb'].','.$r['aus_state'].' '.$r['zip_code'];
            	// we will use this to bold the search word in result
            	$bold           = '<span class="found">' . $searchword . '</span>';    
            	$end_result     .= '<li>' . str_ireplace($searchword, $bold, $result) . '</li>';	
        	}            
        }
        echo $end_result;
    	}else {	
        echo '<li>No results found</li>';
    	}
		
	}    
}
?>