<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
	
?> 

<!DOCTYPE html>
<html>
  <head>
  	
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other 
    	head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
	<title>Live Green</title>
  	
  	<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    
    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">    
    
    
    <style type="text/css">
.bs-example{
	font-family: sans-serif;
	position: relative;
	margin: 50px;
}
.well{
    background: rgba(255, 255, 255, 0.2);
}
.twitter-typeahead{
width:100%;
}

.twitter-typeahead .tt-query,
.twitter-typeahead .tt-hint {
  margin-bottom: 0;
}
.tt-dropdown-menu {
  min-width: 160px;
  margin-top: 2px;
  padding: 5px 0;
  background-color: #fff;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.2);
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
     -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
          box-shadow: 0 5px 10px rgba(0,0,0,.2);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding;
          background-clip: padding-box;
  width:100%;        
}

.tt-suggestion {
  display: block;
  padding: 3px 20px;
}

.tt-suggestion.tt-is-under-cursor {
  color: #fff;
  background-color: #0081c2;
  background-image: -moz-linear-gradient(top, #0088cc, #0077b3);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0077b3));
  background-image: -webkit-linear-gradient(top, #0088cc, #0077b3);
  background-image: -o-linear-gradient(top, #0088cc, #0077b3);
  background-image: linear-gradient(to bottom, #0088cc, #0077b3);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0077b3', GradientType=0)
}

.tt-suggestion.tt-is-under-cursor a {
  color: #fff;
}

.tt-suggestion p {
  margin: 0;
}




body{ font-family:Arial, Helvetica, sans-serif; }
*{ margin:0;padding:0; }
#container { margin: 0 auto; width: 600px; }
a { color:#DF3D82; text-decoration:none }
a:hover { color:#DF3D82; text-decoration:underline; }
ul.update { list-style:none;font-size:1.1em; margin-top:10px }
ul.update li{ height:30px; border-bottom:#dedede solid 1px; text-align:left;}
ul.update li:first-child{ border-top:#dedede solid 1px; height:30px; text-align:left; }
#flash { margin-top:20px; text-align:left; }
#searchresults { text-align:left; margin-top:20px; display:none; font-family:Arial, Helvetica, sans-serif; font-size:16px; color:#000; }
.word { font-weight:bold; color:#000000; }
#search_box { padding:4px; border:solid 1px #666666; width:300px; height:30px; font-size:18px;-moz-border-radius: 6px;-webkit-border-radius: 6px; }
.search_button { border:#000000 solid 1px; padding: 6px; color:#000; font-weight:bold; font-size:16px;-moz-border-radius: 6px;-webkit-border-radius: 6px; }
.found { font-weight: bold; font-style: italic; color: #ff0000; }
h2 { margin-right: 70px; }

</style>
  </head>
  <body>
  	
  	<!-- NAVBAR
    ================================================== -->
	
	<?php @ require_once ("navbar.php"); ?>
	
	
	<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="images/sustainable-housing.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
            	<?php if(isset($_SESSION['user'])) : ?>	
              
              <?php else : ?>
              	
              	<h1>To Add Green Property</h1>
              <p></p>
              <p><a class="btn btn-lg btn-primary" href="register.php" role="button">Sign up today</a></p>
                  		<?php endif; ?>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="images/GreenHouse.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Live Green</h1>
              <p></p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="third-slide" src="images/LiveGreen.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Rent Green</h1>
              <p></p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Rent Green</a></p>
            </div>
          </div>
        </div>
      </div>
	
	<div class="container">
<div class="row">
	
	<form class="col-xs-6 col-md-6" id="searchForm" action="do_findResults.php">
    <fieldset>
    	<div class="well">
        <div class="form-group">
            <input class="form-control" name="query" id="suburb" placeholder="Search by Suburb / Zip Code" type="text">              
        </div>
        <button class="btn btn-success pull-right" type="submit" id="search_button">
        	<span class="glyphicon glyphicon-home"> <strong>Search Property</strong></span>
        </button>
       </div>
    </fieldset>
	</form>

</div>
</div>
	
	<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
    
    
    <div id="searchresults">Search results :</div>
<ul id="results" class="update">
</ul>
 
</div>
     
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>    
    <script src="js/bootstrap.min.js"></script>
    <script src="js/typeahead.min.js"></script>
    <script>
    $(document).ready(function(){
    $('#suburb').typeahead({
        name: 'typeahead',
        minLength: 3,
        remote:{
        	url: 'search.php?key=%QUERY'
        },
        limit : 10
    });
    $('.tt-query').css('background-color','#fff'); 
});
    </script>
    
    
    <script type="text/javascript">
 
$(function() {
 
    $("#search_button").click(function() {
        // getting the value that user typed
        var searchString    = $("#suburb").val();
        // forming the queryString
        var data            = 'search='+ searchString;
         
        // if searchString is not empty
        if(searchString) {
            // ajax call
            $.ajax({
                type: "POST",
                url: "do_findResults.php",
                data: data,
                beforeSend: function(html) { // this happens before actual call
                    $("#results").html(''); 
                    $("#searchresults").show();
                    $(".word").html(searchString);
               },
               success: function(html){ // this happens after we get results
                    $("#results").show();
                    $("#results").append(html);
              }
            });    
        }
        return false;
    });
});
</script>
    
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="js/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>	
	
	
    
  </body>
</html>
