<?php 

    // First we execute our common code to connection to the database and 
    //start or refresh the session 
    require("common.php"); 
	
	$message="";
	// update user's password (Change Password)
	
	if(isset($_POST['submit-password'])){
		
		// This query retreives the user's information from the database using 
        // their id. 
        $query = " 
            SELECT 
                id, username, password, psalt
            FROM users 
            WHERE 
                id = :userID 
        "; 
         
        // The parameter values 
        $query_params = array( 
            ':userID' => $_SESSION['user']['id'] 
        ); 
         
        try 
        { 
            // Execute the query against the database 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) 
        { 
            // Note: On a production website, you should not output $ex->getMessage(). 
            // It may provide an attacker with helpful information about your code.  
            die("Failed to run query: " . $ex->getMessage()); 
        } 
         
        // This variable tells us whether the user has entered the right old password or not. 
        // We initialize it to false, assuming they have not. 
        // If we determine that they have entered the right details, then we switch it to true. 
        $old_password_var = false; 
         
        // Retrieve the user data from the database.  If $row is false, then the old password 
        // they entered is not correct. 
        $row = $stmt->fetch(); 
        if($row) 
        { 
            // Using the password submitted by the user and the salt stored in the database, 
            // we now check to see whether the passwords match by hashing the submitted old password 
            // and comparing it to the hashed version already stored in the database. 
            $check_password = hash('sha256', $_POST['old_password'] . $row['psalt']); 
            for($round = 0; $round < 65536; $round++) 
            { 
                $check_password = hash('sha256', $check_password . $row['psalt']); 
            } 
             
            if($check_password === $row['password']) 
            { 
                // If they do, then we flip this to true 
                $old_password_var = true; 
            } 
        } 
         
        // If the user has successfully entered the old password, then we need to hash the 
        // new password and also check if the new and confirm passwords are matched 
        // Otherwise, we display an error message 
        if($old_password_var) 
        { 
            
			
			
			//check to see if the old password and the new password match
			// Technically they should not be the same; 
			if($_POST['old_password'] == $_POST['password']) {
	    		$message .= "Old Password and New password are the same, Please choose the different new password.<br/> \n\r";
			}
			
			//check to see if the new password and the confirm password match
			if($_POST['password'] != $_POST['password2']) {
	    		$message .= "New Password and Confirm password do not match.<br/> \n\r";
			}
			
			
			// update the password and salt field            
            	$queryPassUpadte="UPDATE users 
            					SET password = :password,
            					psalt=:salt
            					WHERE id = :userID";
				
				// randomnly generate salt	
				$salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
				
				// generate new password using the new salt
				$password = hash('sha256', $_POST['password'] . $salt);
				
				//Next we hash the hash value 65536 more times.  The purpose of this is to 
        		// protect against brute force attacks.  Now an attacker must compute the hash 65537 
        		// times for each guess they make against a password, whereas if the password 
        		// were hashed only once the attacker would have been able to make 65537 different  
        		// guesses in the same amount of time instead of only one.
				for($round = 0; $round < 65536; $round++){ 
            		$password = hash('sha256', $password . $salt); 
        		}
										 
				$query_params_pass_update = array(
					':password' => $password,
					':salt'=>$salt,
					':userID' => $_SESSION['user']['id']
				);
				
				try{
					// Execute the query to update user table
            	$stmtPassUpdate = $db->prepare($queryPassUpadte); 
            	$resultPassUpdate = $stmtPassUpdate->execute($query_params_pass_update); 	
				$message .= "New Password has been updated.<br/> \n\r";			
				
				}catch(PDOException $ex){
					$message.="The password is not updated<br/> \n\r";
				}      
        } 
        else 
        { 
            // Tell the user they failed 
             $message.="The old password is not correct, please try again.<br/> \n\r";
             
       
        } 
		
	}
	
	
?> 

<!DOCTYPE html>
<html>
  <head>
  	
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other 
    	head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
	<title>Change Password</title>
	
	<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    
    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">  
    
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
    
  </head>
   <body>
   	
   	







<body>
  	
<!-- NAVBAR
    ================================================== -->
	
	<?php @ require_once ("navbar.php"); ?>
	
<div class="container" style="padding-top: 60px;" >	
	<h3 class="well">Change Password</h3>
	
	<?php
						if($message != ""){
							echo '<div class="alert alert-success alert-dismissable" id="flash-msg">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									<h4><i class="icon fa fa-check"></i>'.$message.'</h4></div>';
						}
					?>

<div class="col-lg-12 well">
	<div class="row"> 
<form class="form" action="changePassword.php" method="post" id="changePass">  
	
	<div class="form-group">
                          
                          <div class="col-sm-6 col-sm-offset-3">
                              <label for="password"><h4>Old Password</h4></label>
                              <input required="yes" class="form-control" name="old_password" id="old_password" placeholder="Old Password" title="enter your old password." type="password">
                          </div>
                </div>
               
               <div class="form-group">
                          
                          <div class="col-sm-6 col-sm-offset-3">
                              <label for="password"><h4>New Password</h4></label>
                              <input required="yes" class="form-control" name="password" id="password" placeholder="New Password" title="enter your new password." type="password">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-sm-6 col-sm-offset-3">
                            <label for="password2"><h4>Confirm New Password</h4></label>
                              <input required="yes" class="form-control" name="password2" id="password2" placeholder="Confirm New Password" title="enter your new password again." type="password">
                          </div>
                      </div>       	
               	
      <div class="form-group">
           <div class="col-sm-6 col-sm-offset-3">
               <br>
               <button class="btn btn-lg btn-success" type="submit" name="submit-password">
                	<i class="glyphicon glyphicon-ok-sign"></i> Update Password</button>
               <button class="btn btn-lg" type="reset"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
           </div>
      </div>
                      
</form>
</div>
</div>

</div>
<script>
 	$(document).ready(function () {
    $("#flash-msg").delay(3000).fadeOut("slow");
});
 </script>  
 </body>

                    