<?php 

    // First we execute our common code to connection to the database and 
    //start or refresh the session 
    require("common.php"); 
	
	$message="";
	
	// update user details : username and/or email
	if(isset($_POST['submit-userDetails'])){
		
		// initialize variable for form validation
		$success= true;
		
		if(empty($_POST['fullname']) && empty($_POST['email'])){
			$message.="Please enter full name or email to edit details.<br/> \n\r";
			$success=FALSE;
		}		
		
		// check if the email is already in use
		if(!empty($_POST['email'])){
			// Make sure the user entered a valid E-Mail address 
        	// filter_var is a useful PHP function for validating form input, see: 
        	// http://us.php.net/manual/en/function.filter-var.php 
        	// http://us.php.net/manual/en/filter.filters.php 
        	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
        	{ 
          		$message.="Invalid Email Address.<br/> \n\r";  
		  		$success=FALSE; 
        	} 
			
			// Now we perform the same type of check for the email address, in order 
        // to ensure that it is unique. 
        $query = " 
            SELECT 
                1 
            FROM users 
            WHERE 
                email = :email 
        "; 
         
        $query_params = array( 
            ':email' => $_POST['email'] 
        ); 
         
        try 
        { 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) 
        { 
            die("Failed to run query: " . $ex->getMessage()); 
        } 
         
        $row = $stmt->fetch(); 
		
         
        if($row) 
        { 
            $message.="The email is already in use.<br/> \n\r";
			$success=FALSE; 
        } 
		}
		
		// update query for username and email
		if($success && ((!empty($_POST['fullname'])) || (!empty($_POST['email'])))){
			
			// update fullname
			if(!empty($_POST['fullname'])){
				
            // update user table             
            	$queryUpadte="UPDATE users 
            					SET fullname = :fullname
            					WHERE id = :userID"; 
				$query_params_update = array(
					':fullname' => $_POST['fullname'],
					':userID' => $_SESSION['user']['id']
				);
				
				try{
					// Execute the query to update user table
            	$stmtUpdate = $db->prepare($queryUpadte); 
            	$resultUpdate = $stmtUpdate->execute($query_params_update); 				
				$message.="Full Name successfully updated.<br/> \n\r"; 
				}catch(PDOException $ex){
					$message.="The fullname is not updated<br/> \n\r";
				}
			}
			
			// if email is unique, update email
			if(!empty($_POST['email'])){
				// update user table             
            	$queryUpadte="UPDATE users 
            					SET email = :email
            					WHERE id = :userID"; 
				$query_params_update = array(
					':email' => $_POST['email'],
					':userID' => $_SESSION['user']['id']
				);
				
				try{
					// Execute the query to update user table
            	$stmtUpdate = $db->prepare($queryUpadte); 
            	$resultUpdate = $stmtUpdate->execute($query_params_update);
            	$message.="Email successfully updated. Please refresh the page<br/> \n\r"; 
            	
            	
				}catch(PDOException $ex){
					$message.="The email is not updated<br/> \n\r";
				}
			}
			
		}
		
		
	}


	// update user's password (Change Password)
	
	if(isset($_POST['submit-password'])){
		
		// This query retreives the user's information from the database using 
        // their id. 
        $query = " 
            SELECT 
                id, username, password, psalt
            FROM users 
            WHERE 
                id = :userID 
        "; 
         
        // The parameter values 
        $query_params = array( 
            ':userID' => $_SESSION['user']['id'] 
        ); 
         
        try 
        { 
            // Execute the query against the database 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) 
        { 
            // Note: On a production website, you should not output $ex->getMessage(). 
            // It may provide an attacker with helpful information about your code.  
            die("Failed to run query: " . $ex->getMessage()); 
        } 
         
        // This variable tells us whether the user has entered the right old password or not. 
        // We initialize it to false, assuming they have not. 
        // If we determine that they have entered the right details, then we switch it to true. 
        $old_password_var = false; 
         
        // Retrieve the user data from the database.  If $row is false, then the old password 
        // they entered is not correct. 
        $row = $stmt->fetch(); 
        if($row) 
        { 
            // Using the password submitted by the user and the salt stored in the database, 
            // we now check to see whether the passwords match by hashing the submitted old password 
            // and comparing it to the hashed version already stored in the database. 
            $check_password = hash('sha256', $_POST['old_password'] . $row['psalt']); 
            for($round = 0; $round < 65536; $round++) 
            { 
                $check_password = hash('sha256', $check_password . $row['psalt']); 
            } 
             
            if($check_password === $row['password']) 
            { 
                // If they do, then we flip this to true 
                $old_password_var = true; 
            } 
        } 
         
        // If the user has successfully entered the old password, then we need to hash the 
        // new password and also check if the new and confirm passwords are matched 
        // Otherwise, we display an error message 
        if($old_password_var) 
        { 
            
			$message.="The old password is  correct. Hooray!!<br/> \n\r";
			
			//check to see if the old password and the new password match
			// Technically they should not be the same; 
			if($_POST['old_password'] == $_POST['password']) {
	    		$message .= "Old Password and New password are the same, Please choose the different new password.<br/> \n\r";
			}
			
			//check to see if the new password and the confirm password match
			if($_POST['password'] != $_POST['password2']) {
	    		$message .= "New Password and Confirm password do not match.<br/> \n\r";
			}
			
			
			// update the password and salt field            
            	$queryPassUpadte="UPDATE users 
            					SET password = :password,
            					psalt=:salt
            					WHERE id = :userID";
				
				// randomnly generate salt	
				$salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
				
				// generate new password using the new salt
				$password = hash('sha256', $_POST['password'] . $salt);
				
				//Next we hash the hash value 65536 more times.  The purpose of this is to 
        		// protect against brute force attacks.  Now an attacker must compute the hash 65537 
        		// times for each guess they make against a password, whereas if the password 
        		// were hashed only once the attacker would have been able to make 65537 different  
        		// guesses in the same amount of time instead of only one.
				for($round = 0; $round < 65536; $round++){ 
            		$password = hash('sha256', $password . $salt); 
        		}
										 
				$query_params_pass_update = array(
					':password' => $password,
					':salt'=>$salt,
					':userID' => $_SESSION['user']['id']
				);
				
				try{
					// Execute the query to update user table
            	$stmtPassUpdate = $db->prepare($queryPassUpadte); 
            	$resultPassUpdate = $stmtPassUpdate->execute($query_params_pass_update); 	
				$message .= "New Password has been updated.<br/> \n\r";			
				
				}catch(PDOException $ex){
					$message.="The password is not updated<br/> \n\r";
				}      
        } 
        else 
        { 
            // Tell the user they failed 
             $message.="The old password is not correct, please try again.<br/> \n\r";
             
       
        } 
		
	}
	
	
?> 
<!DOCTYPE html>
<html>
  <head>
  	
  	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other 
    	head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    
	<title>Profile</title>
  	
  	<!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    
    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">  
    
    <!-- Font Awesome -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">   
    
    
    
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
    
    <script type="text/javascript" language="javascript" src="js/jquery.js"></script>
		<script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
    <script type="text/javascript" language="javascript" >
			$(document).ready(function() {
				var dataTable = $('#employee-grid').DataTable( {
					"processing": true,
					"serverSide": true,
					"ajax":{
						url :"employee-grid-data.php", // json datasource
						type: "post",  // method  , by default get
						error: function(){  // error handling
							$(".employee-grid-error").html("");
							$("#employee-grid").append
							('<tbody class="employee-grid-error"><tr><th colspan="6">No properties</th></tr></tbody>');
							$("#employee-grid_processing").css("display","none");
							
						}
					}
				} );
			} );
		</script>
		
		
        
    <script src="js/bootstrap.min.js"></script>
    
  </head>
  <body>
  	
<!-- NAVBAR
    ================================================== -->
	
	<?php @ require_once ("navbar.php"); ?>
	
<div class="container" style="padding-top: 60px;" >	
	
	<div class="row">
  		<div class="col-sm-10"><h1>  			
	G'Day, <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?> 			
  		</h1>
  		
  		
  		</div>
    </div>
    <div class="row">
  		<div class="col-sm-3"><!--left col-->
              
          <ul class="list-group">
            <li class="list-group-item text-muted">Profile
            	<a href="javascript:location.reload(true)" 
            	onclick="window.parent.location = window.parent.location.href;">
            		<i class="fa fa-refresh fa-1x"></i></a>
            	</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Joined</strong></span> 
            	<?php    					
    				echo date($_SESSION['user']['join_date']); 				
    			
			?>
            	
            	</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>User Type</strong></span> 
            	<?php     					
    				echo ($_SESSION['user']['user_type']);  				
    			
			?>
            	
            	
            	
            	</li>
            <li class="list-group-item text-right"><span class="pull-left"><strong>Email</strong></span>
            	
					<?php echo ($_SESSION['user']['email']); ?>
            	
            	
            	
            	
            </li>
            
            <li class="list-group-item text-right"><span class="pull-left"><strong>Username</strong></span>
            	
					<?php echo ($_SESSION['user']['username']); ?>
            	
            	
            	
            	
            </li>
            
          </ul> 
               
          <div class="panel panel-default">
            <div class="panel-heading"><a href="add_property.php">Added Properties </a><i class="fa fa-home fa-1x"></i></div>
            <div class="panel-body">
            	
            	<?php echo ($_SESSION['user']['prop_count']);?>
            	
            </div>
          </div>
               
          <div class="panel panel-default">
            <div class="panel-heading">Social Media</div>
            <div class="panel-body">
            	<i class="fa fa-facebook fa-2x"></i> <i class="fa fa-twitter fa-2x"></i> <i class="fa fa-pinterest fa-2x"></i> <i class="fa fa-google-plus fa-2x"></i>
            </div>
          </div>
          
        </div><!--/col-3-->
        
        
        
    	<div class="col-sm-9">
          
          <ul class="nav nav-tabs" id="myTab">
            
            
            <li class="active"><a href="#editProfile" data-toggle="tab">Edit Profile</a></li>
            <li><a href="#addedProperties" data-toggle="tab">Added Properties</a></li>
            
            
          </ul>
              
          <div class="tab-content" >
          	
          	<div class="tab-pane fade" id="addedProperties"  >
          		<table id="employee-grid"  cellpadding="0" cellspacing="0" border="0" class="display" width="100%">
					<thead>
						<tr>
							<th>Apt #</th>
							<th>Street #</th>
							<th>Street Address</th>
							<th>Suburb</th>
							<th>State</th>
							<th>Zip Code</th>
						</tr>
					</thead>
			</table>
          	
			
			</div>
			
            
              
              
             
             
             <div class="tab-pane fade in active" id="editProfile">
            		
            		
            		
            		<?php
						if($message != ""){
							echo '<div class="alert alert-success alert-dismissable" id="flash-msg">
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									<h4><i class="icon fa fa-check"></i>'.$message.'</h4></div>';
						}
					?>
					
					
               	
                  
                  <form class="form" action="user_profile.php" method="post" id="editProfile" onsubmit="return true">
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="full_name"><h4>Full Name</h4></label>
                              <input class="form-control" name="fullname" id="fullname" placeholder="Full Name" type="text">
                          </div>
                      </div>
                      
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <input class="form-control" name="email" id="email" placeholder="you@email.com" title="enter your email." type="email">
                          </div>
                      </div>
                      
                      
                      
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4>Add Phone</h4></label>
                              <input class="form-control" name="phone" id="phone" placeholder="enter phone" title="enter your phone number if any." type="text">
                          </div>
                      </div>
          
                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4>Add Mobile</h4></label>
                              <input class="form-control" name="mobile" id="mobile" placeholder="enter mobile number" title="enter your mobile number if any." type="text">
                          </div>
                      </div>
                      
                      
                     
                      
                      
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit" name="submit-userDetails" 
                              	id="editProfile"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                               	<button class="btn btn-lg" type="reset">
                               		<i class="glyphicon glyphicon-repeat"></i> Reset</button>
                            </div>
                      </div>
              	</form>
              	
              	
              </div>
               
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
    
 <script>
 	$(document).ready(function () {
    $("#flash-msg").delay(3000).fadeOut("slow");
});
 </script>   
    
    </body>
    </html>