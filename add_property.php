<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
	
	// This if statement checks to determine whether the add green property form has been submitted 
    // If it has, then the code is run, otherwise the form is displayed	
	if(isset($_POST['submit-green']))
	{
		
		// An INSERT query is used to add new rows to a database table.
        // Again, we are using special tokens (technically called parameters) to 
        // protect against SQL injection attacks. 
        $query = " 
            INSERT INTO tb_property ( 
            	apt_num,
                street_num, 
                street_route, 
                suburb,
                aus_state,
                zip_code, 
                country_aus,
                id
            ) VALUES ( 
            	:apt_number,
                :st_number, 
                :address,
                :city, 
                :state, 
                :area_code,
                :country,
                :userID
            ) 
        "; 
		
		// Here we prepare our tokens for insertion into the SQL query.  
		
        $query_params = array( 
        	':apt_number' => $_POST['apt_number'],
            ':st_number' => $_POST['st_number'], 
            ':address' => $_POST['address'], 
            ':city'=> $_POST['city'],
            ':state' => $_POST['state'],
            ':area_code'=> $_POST['area_code'],
            ':country' => $_POST['country'],
            ':userID' => $_SESSION['user']['id']          
        );
        
        // If property is added without any error, then 
            // update user table 
            
            	$queryUpadte="UPDATE users 
            					SET prop_count = :propCount
            					WHERE id = :userID"; 
				$query_params_update = array(
					':propCount' => $_SESSION['user']['prop_count']+1,
					':userID' => $_SESSION['user']['id']
				);
				
				
				
				           				
             
		
		try 
        { 
            // Execute the query to add the property 
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
            
            
			// Execute the query to update user table
            $stmtUpdate = $db->prepare($queryUpadte); 
            $resultUpdate = $stmtUpdate->execute($query_params_update); 
                     
        } 
        catch(PDOException $ex) 
        { 
            // Note: On a production website, you should not output $ex->getMessage(). 
            // It may provide an attacker with helpful information about your code.  
            die("Failed to run query: " . $ex->getMessage()); 
        } 
		
	}
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?> 

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Add Green Property</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/checkbox_styles.css">

<script src="js/bootstrap.min.js"></script>



<script>
	// call onload or in script segment below form
function attachCheckboxHandlers() {
    // get reference to element containing features checkboxes
    var el = document.getElementById('features');

    // get reference to input elements in features container element
    var tops = el.getElementsByTagName('input');
    
    
    
    // assign updateTotal function to onclick property of each checkbox
    for (var i=0, len=tops.length; i<len; i++) {
        if ( tops[i].type === 'checkbox' ) {
            tops[i].onclick = updateTotal;
        }
    }
}
    
// called onclick of features checkboxes
function updateTotal(e) {
    // 'this' is reference to checkbox clicked on
    var form = this.form;
    
    // get current value in total text box, using parseFloat since it is a string
    var val = parseFloat( form.elements['total-rating'].value );
    var a;
    
    // if check box is checked, add its value to val, otherwise subtract it
    if ( this.checked ) {
    	
    	a= parseFloat(this.value);
    	a=((a/15.0)*100.0)/20.0;
    	val+=a;
    	
    	
        
        
    } else {
    	a= parseFloat(this.value);
    	a=((a/15.0)*100.0)/20.0;
        val -= a;
    }
    
    
    
    // format val with correct number of decimal places
    // and use it to update value of total text box
    form.elements['total-rating'].value = formatDecimal(val);
    
}
    
// format val to n number of decimal places
// modified version of Danny Goodman's (JS Bible)
function formatDecimal(val, n) {
    n = n || 2;
    var str = "" + Math.round ( parseFloat(val) * Math.pow(10, n) );
    while (str.length <= n) {
        str = "0" + str;
    }
    var pt = str.length - n;
    return str.slice(0,pt) + "." + str.slice(pt);
}

// in script segment below form
attachCheckboxHandlers();
	
</script>
 
   <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet"> 
    
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet"> 
<style type="text/css">
    .bs-example{
    	margin: 30px;
    }
    
    span.stars, span.stars>* {
    display: inline-block;
    background: url(http://i.imgur.com/YsyS5y8.png) 0 -16px repeat-x;
    width: 80px;
    height: 16px;
}
span.stars>*{
    max-width:80px;
    background-position: 0 0;
}
    
</style>



</head>
<body onload="attachCheckboxHandlers()">
	
	
	
	<!-- NAVBAR
    ================================================== -->
	
	<?php @ require_once ("navbar.php"); ?>
	
<div class="bs-example">
	
	<div class="container" style="padding-top: 60px;">
	<div class="row">
    	<div class="col-xs-6 col-md-6 ">
      		<input id="autocomplete" placeholder="Enter your street address to auto-fill the rest of the address fields"
      		       onFocus="geolocate()" type="text" class="form-control"></input>
      	</div>
    
    <div class="dropdown">
    	<div class="col-xs-6 col-md-6 ">    		
  			<div class="btn-group">
    			<a class="btn btn-primary dropdown-toggle" 
    				data-toggle="dropdown" href="#">Select Property Type <span class="caret"></span></a>
    			<ul class="dropdown-menu">
      				<li><a>House</a></li>
      				<li><a>Apartment</a></li>
    			</ul>
  			</div>
		</div>
	</div>
    </div>
    
    
    <form method="post">       	    	        	
    	    	        	
        <div class="row"> 	        	
      <div class="form-group">  
      	   	
         <div class="col-xs-6 col-md-3">
             <label for="apt_num"><h4>Apartment Number</h4></label>
                  <input class="form-control" name="apt_number" id="apartmentNumber" 
                  	placeholder="Apartment Number" title="enter your apartment number." type="text">
         </div>
      
      
      
         <div class="col-xs-6 col-md-3">
             <label for="street"><h4>Street Number</h4></label>
                  <input class="form-control" name="st_number" id="street_number" 
                  	disabled="true" type="text">
                  
         </div>
      
      
      
      
         <div class="col-xs-12 col-md-6">
             
                  <label for="street"><h4>Street Address</h4></label>
                  <input class="form-control" name="address" id="route" 
                  	disabled="true" type="text">
         </div>
      </div>
      </div>
     
     <div class="row">
      <div class="form-group">
      	
         <div class="col-xs-6 col-md-6">
             <label for="city"><h4>City</h4></label>
                  <input class="form-control" name="city" id="locality" 
                  	disabled="true">
         </div>
      
      
      
      
         <div class="col-xs-6 col-md-6">
             <label for="state"><h4>State</h4></label>
                  <input class="form-control" name="state" id="administrative_area_level_1" 
                  	disabled="true">
             
         </div>
      </div>
      </div>
      
      <div class="row">
      <div class="form-group">
      	
         <div class="col-xs-6 col-md-6">
      <label for="zip_code"><h4>Zip Code</h4></label>
                  <input class="form-control" name="area_code" id="postal_code" 
                  	disabled="true">
           
      </div>
      
      
      
         <div class="col-xs-6 col-md-6">
             <label for="country"><h4>Country</h4></label>
                  <input class="form-control" name="country" id="country" 
                  	disabled="true">
         </div>
                  
      </div>
      </div>
      
      <div class="row">
      	<div class="form-group">
      		<div class="col-xs-12 col-md-12">
      			<label for="image"><h4>Property Photo</h4></label>
    			<input type="file" name="property_photo" id="propPhoto">
      		</div>
      	</div>
      </div>
      
      
      
      
      
      
      <br>
      
      
      <div class="panel panel-default" id="features">
                <!-- Default panel contents -->
                <div class="panel-heading">Check Sustainable Features in the property</div>
      
      <div class="row" style="padding: 10px;">
      	<div class="form-group">
      		
      		
      		
			<div class="col-xs-6 col-md-4">
				
				<label for="insulation"><h4>Insulation</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
				<div class="material-switch pull-left" >
                            <input id="someSwitchOptionInsulation" 
                           value="3" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionInsulation" class="label-success" ></label>
                        </div>
                        
			</div>
			
			<div class="col-xs-6 col-md-4">
				<label for="solar_panels"><h4>Solar Panels / Solar Hot Water</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
                        <div class="material-switch pull-left">
                            <input id="someSwitchOptionSolar" 
                           value="3" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionSolar" class="label-success" ></label>
                        </div>
			</div>
			
			<div class="col-xs-6 col-md-4">
				<label for="orientation"><h4>Orientation</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
                        <div class="material-switch pull-left">
                            <input id="someSwitchOptionOrientation" 
                           value="3" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionOrientation" class="label-success" ></label>
                        </div>
			</div>      	
				
      	</div>
      </div>
      
      
      <div class="row" style="padding: 10px;">
      	<div class="form-group" >
      		
      		
      		
			<div class="col-xs-6 col-md-4">
				<label for="double_glazing"><h4>Double Glazing</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
				<div class="material-switch pull-left" >
                            <input id="someSwitchOptionDGlazing" 
                           value="2" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionDGlazing" class="label-success" ></label>
                        </div>                       
			</div>
			
			<div class="col-xs-6 col-md-4">
				<label for="waste_compost"><h4>Waste Compost / Worm Farm</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
                        <div class="material-switch pull-left">
                            <input id="someSwitchOptionCompost" 
                           value="2" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionCompost" class="label-success" ></label>
                        </div>
			</div>
			
			<div class="col-xs-6 col-md-4">
				<label for="ventilation"><h4>Natural ventilation</h4></label>
				<i class="glyphicon glyphicon-question-sign" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."></i>
                        <div class="material-switch pull-left">
                            <input id="someSwitchOptionVentilation" 
                           value="2" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionVentilation" class="label-success" ></label>
                        </div>
			</div> 
			
			<div class="col-xs-12 col-md-4">
      			<label for="total-rating"><h4>Green Stars</h4></label>
      			
      			
      			<input class="input_value" id="textInput"	name="total-rating" size="6"
      			     			 type="text"  value="0.0" readonly="readonly"/>
      			
      		
      			 Rating: <span class="stars" id="nameDiv"></span> stars
      		</div>     	
			
    
    
      	</div>
      </div>
      
      </div>
      
      
      
      
      
               
      <div class="row">
      <div class="form-group">
      	
          <div class="col-xs-12 col-md-12">
          	<hr>
          	<button class="btn btn-lg btn-success" type="submit" name="submit-green">
          		<i class="glyphicon glyphicon-home"></i> Add Green Property</button>
          		
          	<button class="btn btn-lg" type="reset">
          		<i class="glyphicon glyphicon-repeat"></i> Reset</button>
         </div>
      </div>
      </div>
     </form>
</div>
</div>
<script>
// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
var componentForm = {
 street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
// [END region_geolocation]

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDm9Fhn6dFc51glqlpTGSDvMJm9IJSRKG0&signed_in=true&libraries=places&callback=initAutocomplete"
        async defer></script>
        
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script>
$('#features').on('keyup change click paste input propertychange',function(){
    		// Get the value
        var val = parseFloat($('#textInput').val());
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $('#nameDiv').html($span);     		
    	});
	
</script>

<script>
	$(".dropdown-menu li a").click(function(){
  	var selText = $(this).text();
  	$(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
  	if(selText=='House'){
  		document.getElementById('apartmentNumber').disabled=true;
  	}else{
  		document.getElementById('apartmentNumber').disabled=false;
  	}
});
</script>

<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>    
    <script src="js/bootstrap.min.js"></script>
</body>
</html>                                		